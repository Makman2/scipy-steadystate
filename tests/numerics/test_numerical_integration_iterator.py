from scipy_steadystate.itertools import take
from scipy_steadystate.numerics import numerical_integration_iterator


def test_iterator():
    assert take(10, numerical_integration_iterator(lambda t, x: 0, 0, 1, 0)) == [0] * 10
    assert take(10, numerical_integration_iterator(lambda t, x: 0, 11.7, 0.5, 4.2)) == [0] * 10

    assert take(10, numerical_integration_iterator(lambda t, x: t, 0, 1, 0)) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert take(10, numerical_integration_iterator(lambda t, x: t, 7.5, 0.5, 3)) == [3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5]

    assert take(10, numerical_integration_iterator(lambda t, x: x, 0, 1, 0)) == [0] * 10
    assert take(10, numerical_integration_iterator(lambda t, x: x, 0, 0.7, 43.2)) == [0] * 10
    assert take(10, numerical_integration_iterator(lambda t, x: x, 3, 1, 0)) == [3] * 10
    assert take(10, numerical_integration_iterator(lambda t, x: (x + 1), 1, 1, 0)) == [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    assert take(10, numerical_integration_iterator(lambda t, x: 2 * x, 1, 1, -10)) == [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]

    assert take(10, numerical_integration_iterator(lambda t, x: x + t + 1, 1, 1, 0)) == [2, 4, 7, 11, 16, 22, 29, 37, 46, 56]
