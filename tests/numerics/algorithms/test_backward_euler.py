from pytest import approx

from scipy_steadystate.itertools import take
from scipy_steadystate.numerics.algorithms import backward_euler


def test_exponential_series():
    assert take(10, backward_euler(lambda t, x: x, 0, 0.5, 0)) == [0] * 10
    assert take(10, backward_euler(lambda t, x: x, 1, 0.5, 0)) == [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
    assert take(10, backward_euler(lambda t, x: 0.5*x, 1, 0.5, 0)) == approx([1.33333333333333, 1.77777777777778,
                                                                              2.37037037037037, 3.16049382716049,
                                                                              4.21399176954732, 5.61865569272977,
                                                                              7.49154092363969, 9.98872123151959,
                                                                              13.3182949753594, 17.7577266338126])

    assert take(10, backward_euler(lambda t, x: -x, 10, 0.5, 0)) == approx([6.66666666666667, 4.44444444444444,
                                                                            2.96296296296296, 1.97530864197531,
                                                                            1.31687242798354, 0.877914951989026,
                                                                            0.585276634659351, 0.390184423106234,
                                                                            0.260122948737489, 0.173415299158326])
    assert take(10, backward_euler(lambda t, x: -x, 10, 0.25, 0)) == approx([8, 6.4, 5.12, 4.096, 3.2768, 2.62144,
                                                                             2.097152, 1.6777216, 1.34217728,
                                                                             1.073741824])
    assert take(10, backward_euler(lambda t, x: -0.25*x, 1000, 0.25, 0)) == approx([941.176470588235, 885.813148788927,
                                                                                    833.706492977814, 784.664934567354,
                                                                                    738.508173710451, 695.066516433366,
                                                                                    654.180250760815, 615.69905953959,
                                                                                    579.481467801967, 545.394322637146])


def test_lti_time_invariance():
    assert take(10, backward_euler(lambda t, x: x, 0, 0.5, 0)) == take(10, backward_euler(lambda t, x: x, 0, 0.5, 33))
    assert take(10, backward_euler(lambda t, x: x, 1, 0.5, 0)) == take(10, backward_euler(lambda t, x: x, 1, 0.5, 33))
    assert take(10, backward_euler(lambda t, x: 0.5 * x, 1, 0.5, 0)) == take(10, backward_euler(lambda t, x: 0.5 * x, 1, 0.5, 33))

    assert take(10, backward_euler(lambda t, x: -x, 10, 0.5, 0)) == take(10, backward_euler(lambda t, x: -x, 10, 0.5, 33))
    assert take(10, backward_euler(lambda t, x: -x, 10, 0.25, 0)) == take(10, backward_euler(lambda t, x: -x, 10, 0.25, 33))
    assert take(10, backward_euler(lambda t, x: -0.25 * x, 1000, 0.5, 0)) == take(10, backward_euler(lambda t, x: -0.25 * x, 1000, 0.5, 33))


def test_non_lti_system():
    assert take(10, backward_euler(lambda t, x: t, 0, 0.5, 0)) == approx([0.25, 0.75, 1.5, 2.5, 3.75, 5.25, 7, 9, 11.25, 13.75])
    assert take(10, backward_euler(lambda t, x: t, 5, 0.5, 0)) == approx([5.25, 5.75, 6.5, 7.5, 8.75, 10.25, 12, 14, 16.25, 18.75])
    assert take(10, backward_euler(lambda t, x: t, 0, 0.5, 4)) == approx([2.25, 4.75, 7.5, 10.5, 13.75, 17.25, 21, 25, 29.25, 33.75])
    assert take(10, backward_euler(lambda t, x: t, 2, 0.25, 3)) == approx([2.8125, 3.6875, 4.625, 5.625, 6.6875, 7.8125, 9, 10.25, 11.5625, 12.9375])

    assert take(10, backward_euler(lambda t, x: x / t, 2, 0.5, 4)) == approx([2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4, 4.25, 4.5])
