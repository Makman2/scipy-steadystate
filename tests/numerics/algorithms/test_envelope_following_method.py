from math import pi

import numpy as np
import pytest
from pytest import approx
from scipy import signal

from scipy_steadystate.itertools import take
from scipy_steadystate.numerics.algorithms.envelope_following_method import envelope_following_method


@pytest.mark.slow
@pytest.mark.parametrize("skip_method,approximate_solution",
                         [['backward-euler',
                           [(0.22513297, 0.22505815), (0.34634964, 0.34636624), (0.41161538, 0.41169437),
                            (0.45136974, 0.45148827), (0.47057896, 0.47071671), (0.48092163, 0.48106974),
                            (0.48649035, 0.48664404), (0.48926026, 0.48971712), (0.48856147, 0.48874727),
                            (0.48817998, 0.48863876)]],
                          ['trapezoidal',
                           [(0.30622738, 0.30548824), (0.42135833, 0.42157133), (0.46102206, 0.46105056),
                            (0.48575603, 0.48554352), (0.48848438, 0.48893243), (0.49114801, 0.49107248),
                            (0.48870627, 0.48904929), (0.48945647, 0.48973046), (0.48860578, 0.48822254),
                            (0.48974596, 0.49060118)]]])
def test_buck_converter_problem(subtests, skip_method, approximate_solution):
    amp = 10
    f = 1000
    r = 10
    l = 0.1
    c = 1e-6

    T = 1 / f
    omega = 2 * pi * f

    def problem(t, i):
        i1, i3 = i
        E = (1 + signal.square(omega * t)) * amp * 0.5
        return np.array([
            (E - i3 * r) / l,
            (i1 - i3) / (r*c),
        ])

    def jacobian(t, i):
        return np.array([
            [0, -r / l],
            [1 / (r*c), -1 / (r*c)],
        ])

    x0 = np.array([0, 0])
    t0 = 0
    s = 100
    m = 10

    integration_methods = ['midpoint', 'trapezoidal', 'forward-euler', 'backward-euler', 'rk4']

    for integration_method in integration_methods:
        with subtests.test(msg="test different numerical integration methods", integration_method=integration_method):
            efm_integrator = envelope_following_method(problem, x0, T, s, m, t0,
                                                       jac=jacobian,
                                                       skip_method=skip_method,
                                                       integration_method=integration_method)

            i1, i3 = zip(*take(10, efm_integrator))
            i1_expected, i3_expected = zip(*approximate_solution)

            assert i1 == approx(i1_expected, 1e-1)
            assert i3 == approx(i3_expected, 1e-1)
