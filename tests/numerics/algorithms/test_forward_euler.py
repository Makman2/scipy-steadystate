from scipy_steadystate.itertools import take
from scipy_steadystate.numerics.algorithms import forward_euler


def test_exponential_series():
    assert take(10, forward_euler(lambda t, x: x, 0, 1, 0)) == [0] * 10
    assert take(10, forward_euler(lambda t, x: x, 1, 1, 0)) == [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
    assert take(10, forward_euler(lambda t, x: 0.5*x, 1, 1, 0)) == [1.5, 2.25, 3.375, 5.0625, 7.59375, 11.390625,
                                                                    17.0859375, 25.62890625, 38.443359375,
                                                                    57.6650390625]

    assert take(10, forward_euler(lambda t, x: -x, 10, 1, 0)) == [0] * 10
    assert take(10, forward_euler(lambda t, x: -x, 10, 0.5, 0)) == [5, 2.5, 1.25, 0.625, 0.3125, 0.15625, 0.078125,
                                                                    0.0390625, 0.01953125, 0.009765625]
    assert take(10, forward_euler(lambda t, x: -0.25*x, 1000, 0.5, 0)) == [875, 765.625, 669.921875, 586.181640625,
                                                                           512.908935546875, 448.7953186035156,
                                                                           392.6959037780762, 343.60891580581665,
                                                                           300.65780133008957, 263.0755761638284]


def test_lti_time_invariance():
    assert take(10, forward_euler(lambda t, x: x, 0, 1, 0)) == take(10, forward_euler(lambda t, x: x, 0, 1, 33))
    assert take(10, forward_euler(lambda t, x: x, 1, 1, 0)) == take(10, forward_euler(lambda t, x: x, 1, 1, 33))
    assert take(10, forward_euler(lambda t, x: 0.5 * x, 1, 1, 0)) == take(10, forward_euler(lambda t, x: 0.5 * x, 1, 1, 33))

    assert take(10, forward_euler(lambda t, x: -x, 10, 1, 0)) == take(10, forward_euler(lambda t, x: -x, 10, 1, 33))
    assert take(10, forward_euler(lambda t, x: -x, 10, 0.5, 0)) == take(10, forward_euler(lambda t, x: -x, 10, 0.5, 33))
    assert take(10, forward_euler(lambda t, x: -0.25 * x, 1000, 0.5, 0)) == take(10, forward_euler(lambda t, x: -0.25 * x, 1000, 0.5, 33))


def test_non_lti_system():
    assert take(10, forward_euler(lambda t, x: t, 0, 1, 0)) == [0, 1, 3, 6, 10, 15, 21, 28, 36, 45]
    assert take(10, forward_euler(lambda t, x: t, 5, 1, 0)) == [5, 6, 8, 11, 15, 20, 26, 33, 41, 50]
    assert take(10, forward_euler(lambda t, x: t, 0, 1, 4)) == [4, 9, 15, 22, 30, 39, 49, 60, 72, 85]
    assert take(10, forward_euler(lambda t, x: t, 2, 0.5, 3)) == [3.5, 5.25, 7.25, 9.5, 12, 14.75, 17.75, 21, 24.5, 28.25]

    assert take(10, forward_euler(lambda t, x: x / t, 2, 0.5, 4)) == [2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4, 4.25, 4.5]
