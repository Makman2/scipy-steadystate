from pytest import approx

from scipy_steadystate.itertools import take
from scipy_steadystate.numerics.algorithms import midpoint_method


def test_exponential_series():
    assert take(10, midpoint_method(lambda t, x: x, 0, 0.5, 0)) == [0] * 10
    assert take(10, midpoint_method(lambda t, x: x, 1, 1, 0)) == approx([3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049])
    assert take(10, midpoint_method(lambda t, x: 0.5*x, 1, 0.5, 0)) == approx([1.28571428571429, 1.6530612244898,
                                                                               2.12536443148688, 2.7326114119117,
                                                                               3.51335752960076, 4.51717396662955,
                                                                               5.80779509995228, 7.46716512851007,
                                                                               9.60064087951295, 12.3436811308024])

    assert take(10, midpoint_method(lambda t, x: -x, 10, 0.5, 0)) == approx([6, 3.6, 2.16, 1.296, 0.7776, 0.46656,
                                                                             0.279936, 0.1679616, 0.10077696,
                                                                             0.060466176])
    assert take(10, midpoint_method(lambda t, x: -x, 10, 0.25, 0)) == approx([7.77777777777778, 6.04938271604938,
                                                                              4.70507544581619, 3.6595031245237,
                                                                              2.84628020796288, 2.21377349508224,
                                                                              1.72182382950841, 1.33919631183987,
                                                                              1.04159713143101, 0.810131102224121])
    assert take(10, midpoint_method(lambda t, x: -0.25*x, 1000, 0.25, 0)) == approx([939.393939393939, 882.460973370064,
                                                                                     828.978490135515, 778.737369521241,
                                                                                     731.541165307833, 687.205337107358,
                                                                                     645.556528797821, 606.431890688862,
                                                                                     569.678442768325, 535.152476539942])


def test_lti_time_invariance():
    assert take(10, midpoint_method(lambda t, x: x, 0, 0.5, 0)) == take(10, midpoint_method(lambda t, x: x, 0, 0.5, 33))
    assert take(10, midpoint_method(lambda t, x: x, 1, 0.5, 0)) == take(10, midpoint_method(lambda t, x: x, 1, 0.5, 33))
    assert take(10, midpoint_method(lambda t, x: 0.5 * x, 1, 0.5, 0)) == take(10, midpoint_method(lambda t, x: 0.5 * x, 1, 0.5, 33))

    assert take(10, midpoint_method(lambda t, x: -x, 10, 0.5, 0)) == take(10, midpoint_method(lambda t, x: -x, 10, 0.5, 33))
    assert take(10, midpoint_method(lambda t, x: -x, 10, 0.25, 0)) == take(10, midpoint_method(lambda t, x: -x, 10, 0.25, 33))
    assert take(10, midpoint_method(lambda t, x: -0.25 * x, 1000, 0.5, 0)) == take(10, midpoint_method(lambda t, x: -0.25 * x, 1000, 0.5, 33))


def test_non_lti_system():
    assert take(10, midpoint_method(lambda t, x: t, 0, 0.5, 0)) == approx([0.125, 0.5, 1.125, 2, 3.125, 4.5, 6.125, 8,
                                                                           10.125, 12.5])
    assert take(10, midpoint_method(lambda t, x: t, 5, 0.5, 0)) == approx([5.125, 5.5, 6.125, 7, 8.125, 9.5, 11.125, 13,
                                                                           15.125, 17.5])
    assert take(10, midpoint_method(lambda t, x: t, 0, 0.5, 4)) == approx([2.125, 4.5, 7.125, 10, 13.125, 16.5, 20.125,
                                                                           24, 28.125, 32.5])
    assert take(10, midpoint_method(lambda t, x: t, 2, 0.25, 3)) == approx([2.78125, 3.625, 4.53125, 5.5, 6.53125,
                                                                            7.625, 8.78125, 10, 11.28125, 12.625])

    assert take(10, midpoint_method(lambda t, x: x / t, 2, 0.5, 4)) == approx([2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4,
                                                                               4.25, 4.5])
