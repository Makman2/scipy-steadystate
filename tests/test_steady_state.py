from math import sin, pi

import numpy as np
from pytest import approx

from scipy_steadystate import steady_state


def test_electrical_circuit_rc(subtests):
    # One dimensional problem.

    amp = 10
    f = 1000
    T = 1 / f
    omega = 2 * pi * f
    r = 2000
    c = 1e-6

    def problem(t, vc):
        return (amp * sin(omega * t) - vc) / (r * c)

    def jacobian(t, vc):
        return np.array([[-1 / (r * c)]])

    methods = ['midpoint', 'trapezoidal', 'forward-euler', 'backward-euler', 'rk4']
    solutions = [
        steady_state(problem, T=T, x0=np.array([0]), steps=100, jac=jacobian, integration_method=method)
        for method in methods
    ]

    ref_solution = solutions[0][0]

    assert ref_solution == -0.7909004894623738

    for method, solution in zip(methods, solutions):
        with subtests.test(msg="test different numerical integration methods", method=method):
            assert solution[0] == approx(ref_solution, 1e-2)


def test_electrical_electrical_circuit_lrl(subtests):
    # 2-dimensional problem
    amp = 10
    f = 1000
    T = 1 / f
    omega = 2 * pi * f
    r = 100
    l1 = 0.1
    l2 = 0.1

    def problem(t, i):
        i, i2 = i
        return np.array([
            (amp * sin(omega * t) - (i - i2) * r) / l1,
            (i - i2) * r / l2,
        ])

    def jacobian(t, i):
        return np.array([
            [- r / l1, r / l1],
            [r / l2, - r / l2],
        ])

    methods = ['midpoint', 'trapezoidal', 'forward-euler', 'backward-euler', 'rk4']
    x0 = np.array([0.001, -0.001])
    solutions = [
        list(steady_state(problem, T=T, x0=x0, t0=0, steps=100, jac=jacobian, integration_method=method))
        for method in methods
    ]

    ref_solution = solutions[0]

    assert ref_solution == approx([-0.00722726, 0.00722726])

    for method, solution in zip(methods, solutions):
        with subtests.test(msg="test different numerical integration methods", method=method):
            assert solution == approx(ref_solution, 1e1)
